//
//  TYUserDefaults+Properties.h
//  TYUserDefaults
//
//  Created by 夏伟 on 2016/11/8.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYUserDefaults.h"

@interface TYUserDefaults (Properties)

@property (nonatomic, weak) NSString *userName;
@property (nonatomic, weak) NSNumber *userId;
@property (nonatomic, assign) NSInteger integerValue;
@property (nonatomic, assign) BOOL boolValue;
@property (nonatomic, assign) float floatValue;

@end
