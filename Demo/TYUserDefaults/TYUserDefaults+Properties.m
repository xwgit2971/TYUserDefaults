//
//  TYUserDefaults+Properties.m
//  TYUserDefaults
//
//  Created by 夏伟 on 2016/11/8.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYUserDefaults+Properties.h"

@implementation TYUserDefaults (Properties)
@dynamic userName;
@dynamic userId;
@dynamic integerValue;
@dynamic boolValue;
@dynamic floatValue;

@end
