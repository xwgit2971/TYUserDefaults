//
//  ViewController.m
//  TYUserDefaults
//
//  Created by 夏伟 on 2016/11/7.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYUserDefaults+Properties.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // 更详细的用法请参考 https://github.com/gangverk/GVUserDefaults
}

#pragma mark - Actions
- (IBAction)saveAction:(id)sender {
    TYUserDefaults *defaults = [TYUserDefaults standardUserDefaults];
    defaults.userName = @"John";
    [defaults synchronize];
    NSLog(@"存储用户名: %@", @"John");
}

- (IBAction)takeAction:(id)sender {
    TYUserDefaults *defaults = [TYUserDefaults standardUserDefaults];
    NSLog(@"获取用户名: %@", defaults.userName);
}

@end
