//
//  TYUserDefaults.h
//  TYUserDefaults
//
//  Created by 夏伟 on 16/9/23.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <GVUserDefaults/GVUserDefaults.h>

@interface TYUserDefaults : GVUserDefaults

- (void)synchronize;

@end
