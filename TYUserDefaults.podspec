Pod::Spec.new do |s|
  s.name = 'TYUserDefaults'
  s.version = '0.0.1'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'NSUserDefaults access via properties.(基于GVUserDefaults)'
  s.homepage = 'https://gitlab.com/xwgit2971/TYUserDefaults'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYUserDefaults.git', :tag => s.version }
  s.source_files = 'TYUserDefaults/*.{h,m}'
  s.framework = 'Foundation'
  s.requires_arc = true
  s.dependency  "GVUserDefaults", '~> 1.0.2'
end
