# TYUserDefaults
NSUserDefaults access via properties.(基于TYUserDefaults)

## How To Use
Create a category on `TYUserDefaults`, add some properties in the .h file and make them `@dynamic` in the .m file.

// .h
```
@interface TYUserDefaults (Properties)
@property (nonatomic, weak) NSString *userName;
@property (nonatomic, weak) NSNumber *userId;
@property (nonatomic, assign) NSInteger integerValue;
@property (nonatomic, assign) BOOL boolValue;
@property (nonatomic, assign) float floatValue;
@end
```

// .m
```
@implementation TYUserDefaults (Properties)
@dynamic userName;
@dynamic userId;
@dynamic integerValue;
@dynamic boolValue;
@dynamic floatValue;
@end
```

Now, instead of using `[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]`, you can simply use `[TYUserDefaults standardUserDefaults].userName`.

You can even save defaults by setting the property:

```
[TYUserDefaults standardUserDefaults].userName = @"myusername";
[[TYUserDefaults standardUserDefaults] synchronize];
```
